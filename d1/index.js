//Retrieve an element from the webpage
//To retrieve a element use a "#" sign
const txtFirstName = document.querySelector('#txt-first-name');
const txtLastName = document.querySelector('#txt-last-name');
const spanFullName = document.querySelector('#span-full-name');
//"document" refers to the whoe webpage and "querySelector" is used to select a specific object (HTML element) from the document.

/*
	you can use "console.log(document)" para makita nyo na si document ay yung mismong webpage. 

	Alternatively, we can use getElement functions to retrieve the elements
	document.getElementById('txt-first-name');
	document.getElementByClassName('txt-inputs');
	document.getElementByTagName('inputs');
	
*/

//Performs an action when an event triggers
txtFirstName.addEventListener('keyup', (event) => {
	//Using innerHTML para mapasok yung value
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;


	// console.log(event.target);
	// console.log(event.target.value);
})


txtFirstName.addEventListener('keyup', (event) => {
	//spanFullName.innerHTML = txtFirstName.value;

	//"event.target" Contains the element where the event happened
	console.log(event.target);

	//"event.target.value" gets the value of the input object
	console.log(event.target.value);

	// console.log(document.getElementById('#txt-first-name'))
})

txtLastName.addEventListener('keyup', (event) => {
	//Using innerHTML para mapasok yung value
	spanFullName.innerHTML = txtFirstName.value + ' ' + txtLastName.value;

	// console.log(event.target);
	// console.log(event.target.value);
})


txtLastName.addEventListener('keyup', (event) => {
	//spanFullName.innerHTML = txtFirstName.value;

	//"event.target" Contains the element where the event happened
	console.log(event.target);

	//"event.target.value" gets the value of the input object
	console.log(event.target.value);
})
